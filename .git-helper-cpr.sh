#!/bin/bash
echo "Enter the starting commit hash: "
read hash1
echo "Enter the ending commit hash: "
read hash2
git cherry-pick ${hash1}^..${hash2}
