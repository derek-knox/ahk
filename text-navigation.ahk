#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

; Cursor movement

#left::send {Home}
#right::send {End}
#up::send {Alt down}{Up down}{Up up}{Alt up}				        ; Override OS default -> VSCode shortcut delegation
#down::send {Alt down}{Down down}{Down up}{Alt up}			        ; Override OS default -> VSCode shortcut delegation
^#space::send {Control down}{i down}{i up}{Control up}{Escape}		; Override OS default -> VSCode shortcut delegation
^Esc::send {Control down}{i down}{i up}{Control up}{Escape}		    ; Override OS default -> VSCode shortcut delegation
#del::
    SendInput, {Home} ; Move to the start of the line
    SendInput, +{End} ; Select the entire line
    SendInput, {Del} ; Delete the selected line
    SendInput, {Up} ; Move up to the previous line
    SendInput, {End} ; Move to the end of the line
    SendInput, +{Down} ; Select the line break
    SendInput, {Del} ; Remove the line break, joining the lines
return
#enter::send {Escape}{End}{Enter}
#space::send {Escape}{right 1}
^#left::send {left 25}                                              ; May need to comment out on Win 11 to prevent Window Shorty Clash
^#right::send {right 25}                                            ; May need to comment out on Win 11 to prevent Window Shorty Clash

; Text selection

+#left::send {Shift down}{Home}{Shift up}
+#right::send {Shift down}{End}{Shift up}

; Text insertion

#-::send {Asc 0151}
