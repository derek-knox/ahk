#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

; Unity
#u::
if WinExist("ahk_exe Unity.exe")
    WinActivate
    WinWaitActive Unity,, 1
return

; Unity w/Autoplay
^#u::
if WinExist("ahk_exe Unity.exe")
    WinActivate
    WinWaitActive Unity,, 1
    Sleep 750
    Send ^{space} ; Auto play shortcut in Unity
return

; VSCode
#c::
if WinExist("ahk_exe Code.exe")
    WinActivate
    WinWaitActive Code,, 1
return
