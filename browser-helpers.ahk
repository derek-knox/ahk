#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

; Chrome quick search - assumes prerequisite of a text selection
#s::
if WinExist("ahk_exe Chrome.exe")
    send {Control down}{c down}{c up}{Control up}       ; copy
    WinActivate
    send {Control down}{t down}{t up}{Control up}       ; new tab
    send {Control down}{v down}{v up}{Control up}       ; paste
    send {Enter}                                        ; search
return
